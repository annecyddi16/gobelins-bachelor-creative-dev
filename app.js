/**
 * Created by Celine on 17/11/2016.
 */
var express = require('express');
var path = require('path');
var app = express();
var _ = require('lodash');

var server = require('http').createServer(app);
var io = require('socket.io')(server);

app.get('/', function(req, res){
    res.sendfile('index.html');
});

var users = [];


app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', function (socket) {
    console.log('connexion');

    socket.on('currentUser', function(spiral){
        console.log('Spiral: ' + spiral);
        users.push(spiral);
        console.log(users);
        io.emit('get currentUser', spiral);
        io.emit('get users', users);
    });
});

server.listen(3000, '172.28.59.58');