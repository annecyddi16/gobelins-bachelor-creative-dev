var tabParam = [
    [2,          "rgba(3,221,179,1)", 2, 0, 0, 60, "rond", "rgba(37,37,37,1)"],
    [0.5,        "#f73b88" , 1, 0, 120, 7, "rond", "rgba(37,37,37,1)"],
    [0.5,        "rgba(188,16,111,0.5)" , 1, 0, 120, 7, "line", "#040403"],
    [10,         "#faf342" , 10, 0, 0, 3, "line", "rgba(37,37,37,1)"],
    [3,          "#de131e" , 3, 4, 120, 3, "line", "rgba(37,37,37,1)"]
];

var spiral = (function(){

    /****************
     * Object Spiral
     */
    var Spiral = function (){

        this.canvas = null;
        this.context = null;
        this.radius = null;
        this.angle = null;
        this.randomVariance = null;
        this.wobble = null;
        this.width = null;
        this.height = null;
        this.stepSize = null;
        this.x = null;
        this.y = null;
        this.type = null;
        this.index = null;
    };

    Spiral.prototype.init = function(container, index, width, height){
        this.createCanvas(container, width, height);

        this.context = this.canvas.getContext("2d");

        this.setUpParam(container, index);

        requestAnimationFrame(this.drawingSpiral.bind(this));
    };

    Spiral.prototype.createCanvas = function(container, width, height){
        this.canvas = document.createElement('canvas');
        this.canvas.width = width;
        this.canvas.height = height;

        $(this.canvas).appendTo($(container));
    };


    Spiral.prototype.setUpParam = function(container, index){
        this.context.lineWidth = tabParam[index][0];
        this.context.strokeStyle = tabParam[index][1];
        this.radius = 0;
        this.radiusStep = tabParam[index][2];
        this.angle = 0;
        this.randomVariance = tabParam[index][3];
        this.wobble = tabParam[index][4];
        this.stepSize = tabParam[index][5];
        this.type = tabParam[index][6];

        $(container).css('background-color', tabParam[index][7]);

        this.context.beginPath();
        this.context.moveTo($(this.canvas).width()/2, $(this.canvas).height()/2);

        this.clearRect();
    };

    Spiral.prototype.drawingSpiral = function(){
        this.index++;
        this.radius += this.radiusStep;

        this.wobbleSpiral();

        this.angle += (Math.PI * 2) / this.stepSize;

        this.x = $(this.canvas).width() / 2 + this.radius * Math.cos(this.angle);
        this.y = $(this.canvas).height() / 2 + this.radius * Math.sin(this.angle);

        this.varianceSpiral();
        this.drawType(this.x, this.y);

        if(this.index < 1000) {requestAnimationFrame(this.drawingSpiral.bind(this))};
    };

    Spiral.prototype.drawType = function(x, y){
        if (this.type == "line") {
            this.context.lineTo(x, y);
            this.context.stroke();
        }
        if (this.type == "rond") {
            this.context.arc(x, y, 2, 0, 2 * Math.PI);
            this.context.stroke();
            this.context.closePath();
        }
    };

    Spiral.prototype.wobbleSpiral = function(){
        if (this.angle < 0) this.angle += this.wobble;
        if (this.angle >= this.wobble) this.angle -= this.wobble;
    };


    Spiral.prototype.varianceSpiral = function(){
        this.x += Math.floor(Math.random() * ((this.randomVariance - (-1 * this.randomVariance)) + 1) + (-1 * this.randomVariance));
        this.x += Math.floor(Math.random() * ((this.randomVariance - (-1 * this.randomVariance)) + 1) + (-1 * this.randomVariance));
    };

    Spiral.prototype.clearRect = function(){
        this.context.clearRect(0, 0, $(this.canvas).width(), $(this.canvas).height());
    };

    return Spiral;
})();