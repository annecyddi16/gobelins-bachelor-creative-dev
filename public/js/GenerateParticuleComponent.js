var paramCurentUser = [];
var nbParticule = 300;
var distance = 1000;
var min = -1000;
var max = 1000;
var randomColor;
var taille = 0.5;

var TabParticule = [];
var TabVitesse = [];
for(var i=0; i<nbParticule; i++) TabVitesse[i]=[];

var container;
var renderer;
var scene;
var camera;


function generateParticules(nb) {

    var i;
    for (i = 0; i < nb; i++) {

        TabParticule[i] = new THREE.Particle(new THREE.ParticleCanvasMaterial({
            color: 0xffffff,
            opacity: Math.random(),
            program: function (context) {
                context.beginPath();
                context.arc(0, 0, taille, 0, Math.PI * 2, true);
                context.closePath();
                context.fill();
            }

        }));

        initializeParticulePosition(TabParticule[i]);
        setUpRandomScale(TabParticule[i]);
        setUpRandomSpeed();

        scene.add(TabParticule[i]);
    }
    renderCanvas();
}

function initializeParticulePosition(el){
    el.position.x = Math.random() * distance * 2 - distance;
    el.position.y = Math.random() * distance * 2 - distance;
    el.position.z = Math.random() * distance * 2 - distance;
}


function setUpRandomSpeed() {
    var i;
    for (i = 0; i < nbParticule; i++) {
        TabVitesse[i][0] = Math.round((Math.random() - 0.5) * 10);
        TabVitesse[i][1] = Math.round((Math.random() - 0.5) * 10);
        TabVitesse[i][2] = Math.round((Math.random() - 0.5) * 10);
    }
}

function setUpRandomScale(el) {
    el.scale.x = el.scale.y = Math.random() * 10 + 5;
}

function renderCanvas() {
    renderer.render(scene, camera);
}

function moveParticules() {

    var i;
    for (i = 0; i < nbParticule; i++) {

        //Condition de rebond
        if ((TabParticule[i].position.x > max) && (TabVitesse[i][0] > 0))
            TabVitesse[i][0] *= -1;

        if ((TabParticule[i].position.y > max) && (TabVitesse[i][1] > 0))
            TabVitesse[i][1] *= -1;

        if ((TabParticule[i].position.z > max) && (TabVitesse[i][2] > 0))
            TabVitesse[i][2] *= -1;

        if ((TabParticule[i].position.x < min) && (TabVitesse[i][0] < 0))
            TabVitesse[i][0] *= -1;

        if ((TabParticule[i].position.y < min) && (TabVitesse[i][1] < 0))
            TabVitesse[i][1] *= -1;

        if ((TabParticule[i].position.z < min) && (TabVitesse[i][2] < 0))
            TabVitesse[i][3] *= -1;

        //On fait avancer
        TabParticule[i].position.x += TabVitesse[i][0];
        TabParticule[i].position.y += TabVitesse[i][1];
        TabParticule[i].position.z += TabVitesse[i][2];
    }

    renderCanvas();
    requestAnimationFrame(moveParticules);
}

function setUpGenerateParticuleComponent(){
    /* Set up Canvas */
    container = document.getElementById('App_ContainerCanvas');
    renderer = new THREE.CanvasRenderer();
    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);
    scene.add(camera);

    generateParticules(nbParticule);
    camera.position.z = 100;
    camera.lookAt(camera);
    renderer.render(scene, camera);
    requestAnimationFrame(moveParticules);
}