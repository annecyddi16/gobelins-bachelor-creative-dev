var currentUser = [];

var app = (function() {

	/****************
	 * Object App
	 */
	var App = {
		currentSpiral : null,

		init: function(){
			setUpGenerateParticuleComponent();
			this.animButton();
			this.listen();
		},
		animButton: function(){
			TweenMax.to($('.App_SplashScreen_buttonTop'), 0.3, {width: 300, ease:  Circ.easeOut, delay: 0.3});
			TweenMax.to($('.App_SplashScreen_buttonRight'), 0.3, {height: "100%", ease:  Circ.easeOut, delay: 0.6});
			TweenMax.to($('.App_SplashScreen_buttonBottom'), 0.3, {width: 300, ease:  Circ.easeOut, delay: 0.9});
			TweenMax.to($('.App_SplashScreen_buttonLeft'), 0.3, {height: "100%", ease:  Circ.easeOut, delay: 0.12});
		},
		listen: function(){
			$('.App_Slide:nth-child(2) .App_SplashScreen_button').on('click', this.firstChapter.bind(this));
			$('#App_submit').on('click', this.secondChapter.bind(this));
			$('.App_Canvas_button').on('click', this.thirdChapter.bind(this));
		},
		firstChapter: function(e){
			console.log(e.target);
			this.changeChapter($('.App_Slide:nth-child(2)'), $('.App_Slide:nth-child(3)'));
		},
		secondChapter: function(e){
			e.preventDefault();

			this.getParam();
			this.changeChapter($('.App_Slide:nth-child(3)'), $('.App_Slide:nth-child(4)'));
			this.hiddeBackground();

			socket.emit('currentUser', this.currentSpiral);

			var mySpiral = new spiral();
			mySpiral.init("#App_ContainerMainCanvas", this.currentSpiral, window.innerWidth, window.innerHeight);
		},
		thirdChapter: function(){
			this.changeChapter($('.App_Slide:nth-child(4)'), $('.App_Slide:nth-child(5)'));
			this.visibleBackground();
		},
		getParam: function(){
			this.pseudoTest($('#pseudo').val());
			this.ageTest($('#age').val());
			this.tailleTest($('#taille').val());
			this.cheveuxTest($('#hair').val());
			this.likeTest($('#like').val());
		},
		changeChapter: function(slide1, slide2){
			slide1.removeClass("App_Slide-active");
			slide2.addClass("App_Slide-active");
		},
		hiddeBackground: function(){
			$('.App_Background').addClass("App_Background-hidden");
		},
		visibleBackground: function(){
			$('.App_Background').removeClass("App_Background-hidden");
		},
		pseudoTest:function(pseudo){
			if(pseudo.length < 5){
				this.currentSpiral++;
			}
			console.log(this.currentSpiral);
		},
		ageTest:function(age){
			if(age < 18){
				this.currentSpiral++;
			}
			console.log(this.currentSpiral);
		},
		tailleTest:function(taille) {
			if (taille < 175) {
				this.currentSpiral++;
			}
			console.log(this.currentSpiral);
		},
		cheveuxTest:function(cheveux) {
			if (cheveux == 1) {
				this.currentSpiral++;
			}
			if (cheveux == 2) {
				this.currentSpiral++;
			}
			if (cheveux == 3) {
				this.currentSpiral = this.currentSpiral +0;
			}
			if (cheveux == 4) {
				this.currentSpiral = this.currentSpiral +0;
			}
			console.log(this.currentSpiral);
		},
		likeTest:function(like) {
			if (like == 1) {
				this.currentSpiral = this.currentSpiral +0;
			}
			if (like == 2) {
				this.currentSpiral++;
			}
			if (like == 3) {
				this.currentSpiral++;
			}
			if (like == 4) {
				this.currentSpiral = this.currentSpiral +0;
			}
			console.log(this.currentSpiral);
		}

	};

	return App;
})();

app.init();






