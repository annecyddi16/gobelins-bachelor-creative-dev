![Capture.PNG](https://bitbucket.org/repo/ad45K5/images/308995237-Capture.PNG)

![Capture1.PNG](https://bitbucket.org/repo/ad45K5/images/2652157551-Capture1.PNG)

### Install

```
#!javascript
npm install
npm start

```

### Sujet ###

* Spirale

### Développeur(s) ###
* Céline Chamiot-Poncet

### Notes ###
* Ne pas oublier de faire un npm install et un node app.js
* Pour info le composant en three.js n'est pas en javascript object car this.renderer.render(this.scene, this.camera); marchait très mal

### Principe ###
* Création de spirales personnalisées en fonction de réponses à un questionnaire 

### Technos / Libs ###

* GSAP
* Three.js
* Socket IO